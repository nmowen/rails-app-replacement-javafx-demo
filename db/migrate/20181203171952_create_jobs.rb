class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs do |t|
      t.string :name
      t.references :employee, foreign_key: true
      t.references :customer, foreign_key: true
      t.references :stage, foreign_key: true
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
