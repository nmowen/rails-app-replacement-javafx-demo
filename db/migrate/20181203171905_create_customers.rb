class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :state_abbr
      t.string :zip_code
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
