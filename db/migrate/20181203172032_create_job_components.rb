class CreateJobComponents < ActiveRecord::Migration[5.1]
  def change
    create_table :job_components do |t|
      t.references :job, foreign_key: true
      t.references :component, foreign_key: true
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
