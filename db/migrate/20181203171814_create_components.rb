class CreateComponents < ActiveRecord::Migration[5.1]
  def change
    create_table :components do |t|
      t.string :name
      t.float :price
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
