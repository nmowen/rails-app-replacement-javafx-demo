class AddTotalSubtotalToJob < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :total, :float
    add_column :jobs, :subtotal, :float
  end
end
