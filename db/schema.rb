# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181203181902) do

  create_table "components", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["status_id"], name: "index_components_on_status_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "city"
    t.string "state_abbr"
    t.string "zip_code"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["status_id"], name: "index_customers_on_status_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["status_id"], name: "index_employees_on_status_id"
  end

  create_table "job_components", force: :cascade do |t|
    t.integer "job_id"
    t.integer "component_id"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["component_id"], name: "index_job_components_on_component_id"
    t.index ["job_id"], name: "index_job_components_on_job_id"
    t.index ["status_id"], name: "index_job_components_on_status_id"
  end

  create_table "jobs", force: :cascade do |t|
    t.string "name"
    t.integer "employee_id"
    t.integer "customer_id"
    t.integer "stage_id"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "total"
    t.float "subtotal"
    t.index ["customer_id"], name: "index_jobs_on_customer_id"
    t.index ["employee_id"], name: "index_jobs_on_employee_id"
    t.index ["stage_id"], name: "index_jobs_on_stage_id"
    t.index ["status_id"], name: "index_jobs_on_status_id"
  end

  create_table "stages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
