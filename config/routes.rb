Rails.application.routes.draw do
  root 'jobs#index'
  resources :job_components
  resources :jobs
  resources :customers
  resources :components
  resources :employees
  resources :statuses
  resources :stages
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
