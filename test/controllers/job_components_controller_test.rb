require 'test_helper'

class JobComponentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @job_component = job_components(:one)
  end

  test "should get index" do
    get job_components_url
    assert_response :success
  end

  test "should get new" do
    get new_job_component_url
    assert_response :success
  end

  test "should create job_component" do
    assert_difference('JobComponent.count') do
      post job_components_url, params: { job_component: { component_id: @job_component.component_id, job_id: @job_component.job_id, status_id: @job_component.status_id } }
    end

    assert_redirected_to job_component_url(JobComponent.last)
  end

  test "should show job_component" do
    get job_component_url(@job_component)
    assert_response :success
  end

  test "should get edit" do
    get edit_job_component_url(@job_component)
    assert_response :success
  end

  test "should update job_component" do
    patch job_component_url(@job_component), params: { job_component: { component_id: @job_component.component_id, job_id: @job_component.job_id, status_id: @job_component.status_id } }
    assert_redirected_to job_component_url(@job_component)
  end

  test "should destroy job_component" do
    assert_difference('JobComponent.count', -1) do
      delete job_component_url(@job_component)
    end

    assert_redirected_to job_components_url
  end
end
