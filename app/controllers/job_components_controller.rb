class JobComponentsController < ApplicationController
  before_action :set_job_component, only: [:show, :edit, :update, :destroy]

  # GET /job_components
  # GET /job_components.json
  def index
    @job_components = JobComponent.all
  end

  # GET /job_components/1
  # GET /job_components/1.json
  def show
  end

  # GET /job_components/new
  def new
    @job_component = JobComponent.new
  end

  # GET /job_components/1/edit
  def edit
  end

  # POST /job_components
  # POST /job_components.json
  def create
    @job_component = JobComponent.new(job_component_params)

    respond_to do |format|
      if @job_component.save
        format.html { redirect_to @job_component, notice: 'Job component was successfully created.' }
        format.json { render :show, status: :created, location: @job_component }
      else
        format.html { render :new }
        format.json { render json: @job_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_components/1
  # PATCH/PUT /job_components/1.json
  def update
    respond_to do |format|
      if @job_component.update(job_component_params)
        format.html { redirect_to @job_component, notice: 'Job component was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_component }
      else
        format.html { render :edit }
        format.json { render json: @job_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_components/1
  # DELETE /job_components/1.json
  def destroy
    @job_component.destroy
    respond_to do |format|
      format.html { redirect_to job_components_url, notice: 'Job component was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_component
      @job_component = JobComponent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_component_params
      params.require(:job_component).permit(:job_id, :component_id, :status_id)
    end
end
