json.extract! job, :id, :name, :employee_id, :customer_id, :stage_id, :status_id, :created_at, :updated_at
json.url job_url(job, format: :json)
