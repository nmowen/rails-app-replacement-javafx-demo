json.extract! customer, :id, :name, :address, :city, :state_abbr, :zip_code, :status_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
