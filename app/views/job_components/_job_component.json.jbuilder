json.extract! job_component, :id, :job_id, :component_id, :status_id, :created_at, :updated_at
json.url job_component_url(job_component, format: :json)
