json.extract! employee, :id, :name, :status_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
