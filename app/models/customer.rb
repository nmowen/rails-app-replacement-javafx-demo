class Customer < ApplicationRecord
  belongs_to :status
  has_many :jobs

  validates :name, :address, :city, :state_abbr, :zip_code, :status_id, presence: true
  validates_length_of :state_abbr, is: 2, allow_blank: false
end
