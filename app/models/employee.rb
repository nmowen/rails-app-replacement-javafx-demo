class Employee < ApplicationRecord
  belongs_to :status

  validates :name, :status_id, presence: true
end
