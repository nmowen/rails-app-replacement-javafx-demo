class Job < ApplicationRecord
  belongs_to :employee
  belongs_to :customer
  belongs_to :stage
  belongs_to :status
  has_many :job_components
  has_many :components, through: :job_components

  validates :name, :employee_id, :customer_id, :stage_id, :status_id, presence: true
  after_save :complete_math

  def complete_math
    subtotalZ = self.job_components.joins(:component).sum(:price)
    taxE = 0.034
    markup = 0.1
    multiY = subtotalZ * taxE
    multiZ = subtotalZ * markup
    c = subtotalZ + multiY + multiZ
    update_columns(subtotal:subtotalZ, total: c)
  end
end
