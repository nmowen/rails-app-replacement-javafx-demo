class JobComponent < ApplicationRecord
  belongs_to :job
  belongs_to :component
  belongs_to :status

  validates :job_id, :component_id, presence: true
end
