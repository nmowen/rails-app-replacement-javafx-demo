class Component < ApplicationRecord
  belongs_to :status
  has_many :job_components
  validates :name, :price, presence: true
  validates :price, numericality: { greater_than: 0 }
end
